<?php

$depths = file_get_contents("input.txt");
$depths = explode("\n", trim($depths));
$count = 0;
for ($i = 0; $i < sizeof($depths); $i++) {
  if ($i > 2) {
    $compare00 = $depths[$i-3] + $depths[$i-2] + $depths[$i-1];
    $compare01 = $depths[$i-2] + $depths[$i-1] + $depths[$i];
    if ($compare01 - $compare00 > 0) {
      $count++;
    }
  }
}
echo "Count: " . $count . "\n";

?>
