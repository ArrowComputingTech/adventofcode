<?php

$commands = file_get_contents("input.txt");

function iterateOver($arr) {
  $horizontalPosition = 0;
  $depth = 0;
  $aim = 0;
  for ($i = 0; $i < sizeof($arr); $i++) {
    $data = explode(" ", $arr[$i]);
    switch ($data[0]) {
      case "forward":
        $horizontalPosition += $data[1];
        $depth += ($aim * $data[1]);
        break;
      case "up":
        $aim -= $data[1];
        break;
      case "down":
        $aim += $data[1];
        break;
    }
    if ($depth < 0) {
      $depth = 0;
    }
  }
  return "HPos * depth: " . $depth * $horizontalPosition . "\n";
}

$comsplode = explode("\n", trim($commands));

echo iterateOver($comsplode);
