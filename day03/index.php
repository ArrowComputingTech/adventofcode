<?php


function getColumns($lineArr) {
  $columns = array();
  for ($i = 0; $i < sizeof($lineArr); $i++) {
    $chrs[$i] = str_split($lineArr[$i]);
    for ($j = 0; $j < sizeof($chrs[$i]); $j++) {
      if ($i == 0) {
        $columns[$j][0] = 0;
        $columns[$j][1] = 0;
      }
      if ($chrs[$i][$j] == 0) {
        $columns[$j][0] += 1;
      } else {
        $columns[$j][1] += 1;
      }
    }
  }
  return $columns;
}

function getMaxDec($columns) {
  $digit = "";
  foreach ($columns as $col) {
    if ($col[0] > $col[1]) {
      $digit .= "0";
    } else {
      $digit .= "1";
    }
  }
  return bindec($digit);
}

function getMinDec($columns) {
  $digit = "";
  foreach ($columns as $col) {
    if ($col[0] < $col[1]) {
      $digit .= "0";
    } else {
      $digit .= "1";
    }
  }
  return bindec($digit);
}

$lineArr = explode("\n", trim(file_get_contents("input.txt")));
$columns = getColumns($lineArr);
$gamma = getMaxDec($columns);
$epsilon = getMinDec($columns);

echo "Gamma: " . $gamma;
echo PHP_EOL;
echo "Epsilon: " . $epsilon;
echo PHP_EOL;
echo "Power consumption: " . $gamma * $epsilon;
echo PHP_EOL;
